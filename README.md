# GRUB2 setup

This is a sample GRUB2 configuration setup to be placed on a bootable USB drive or hard disk with live CD images, allowing for multiple systems to be booted (multiboot).

Will neither boot every live CD image there is nor provide every single option available for an image.
Intent is to be able to boot images for normal daily use, with a few extras for special use cases.
*Be prepared to manually customize the boot setup*, and thus know or learn GRUB2 configuration.
See projects like these if you simply want to boot a number of live CD images with default settings that might not sit well for daily use:

- [multibooty](https://gitlab.com/alasdairkeyes/multibooty)
- [GRUB2 Live ISO Multiboot (GLIM)](https://github.com/thias/glim)

This project's focus is on [research and documentation](#more-information), especially with regards to non-standard usage
(e.g. using options not on the entries included with the live CD images and using the same setup for both bare-metal and VM runs).

The "boot ISO live CD image within a partition" is already non-standard, so expect some bells and whistles to be limited.

## Features

`conf.d/liventry.cfg` lists entries for booting ISO files with different options.

`conf.d/grubconf.cfg` scans system for other grub.cfg files to jump to.  Handy when booted into the wrong GRUB2 setup.  Shows additional entries to display partition information and grub_cfg.txt to aid in identification of the GRUB2 configuration file.

`conf.d/grub4dos.cfg` boots GRUB4DOS.

## How to use

The setup assumes that GRUB2 files will live on a FAT32 filesystem, and ISOs in an ext2/3/4 filesystem.

1. Install GRUB2
1. Place ISO files
1. Add files in this project into GRUB2 directory
   - see note below regarding `grub.cfg`
1. Adjust configuration
   - `conf.d/liventry.cfg`: GRUB2 menu entries
      - adjust path to ISO files
         - assumes ISO files placed in `/iso`
         - may prefer `/boot/iso` as used in other projects
            - other projects put ISOs in same partition as GRUB so directory is grouped
      - may use `conf.d/liventry.local.cfg` to completely bypass existing entries
   - `conf.d/livecd.cfg`: alternative to `conf.d/liventry.cfg`
      - no up-front scanning
         - loads faster
         - won't complain about missing files
      - modify `include_list` in `conf.d/variable.cfg` to use
   - `conf.d/variable.cfg`: setup-dependent variables
      - `livecd_dev`: set to GRUB2 partition name for where ISO resides, without parentheses
      - `livecd_uuid`: set to UUID of partition where ISO resides
         - Will override `livecd_dev` if different
      - `grub4dos_uuid`: set to UUID of partition where GRUB4DOS resides
      - may use `conf.d/variable.local.cfg` to add additional changes on top
   - `conf.d/txtcolor.cfg`: menu colors
      - note: variables not exported so will only affect main menu
   - `grub_cfg.txt`: description of GRUB2 setup for display by `conf.d/grubconf.cfg`
   - `conf.d/cmos.cfg` and `conf.d/flagusb.cfg`: USB device reordering
      - used by Knoppix entry to adjust ISO-containing device hint
         - optional; Knoppix will do fallback search that may have side effects
      - need to have a flag in CMOS indicating USB boot
         - update `conf.d/cmos.cfg` with bit to test for
         - can hard-code it when set up on USB device
      - most likely not usable, but can include
         - modify `include_list` in `conf.d/variable.cfg` to use 
1. Set up persistence
   - partition
   - file
      - supported by systems that allow custom path boot option
         - so far only supported in limited cases
            - `casper`-based systems
            - `live-boot`-based systems
         - See [wiki](https://gitlab.com/serenascopycat/grub-config/-/wikis/persist) for more information
      - not supported for systems that assume persistent files are "inside ISO image"
         - e.g.
            - `dracut`-based systems
            - Android x86
         - assumes ISO directly written out to writable media
            - normal use case of "burn-to-USB-drive" hybrid (MBR + ISO9660) ISOs
               - read-only loop-mounted ISO prevents it from working
      - not fully supported
         - Knoppix
            - no reliable way to refer to specific partitions
               - unstable device names
               - does not support mounting by label/UUID
               - can use fixed mount points used for booting system
                  - limits which partitions the files can be at
            - mounted read-only
               - may be possible to manually save files and have them appear after reboot

Note: instead of using `grub.cfg`, which is mostly generated, can take only the important bits from it:

```
set "config_file_root=${config_file}"
export config_file_root
if [ -f "${config_directory}/conf.d/_include.cfg" ]; then
  source "${config_directory}/conf.d/_include.cfg"
elif [ -z "${config_directory}" -a -f "${prefix}/conf.d/_include.cfg" ]; then
  source "${prefix}/conf.d/_include.cfg";
fi
```

The first couple of lines setting up `config_file_root` is used by `conf.d/grubconf.cfg`.  The rest is to source the starting configuration file `conf.d/_include.cfg`.

## Issues

When getting "invalid extent" errors while in GRUB in 32-bit mode, make sure the partition where the ISO files are is not too large (too many extents) to be addressed by 32 bits.
If doing something like `ls (hd0,msdos3)/iso` yields the error, try using a smaller partition.

## Crash course

`conf.d/liventry.cfg` lists the live CD entries, using `conf.d/livetmpl.cfg` and `conf.d/livefunc.cfg` to verify ISO file presence and set up the entries.  This approach does slow down processing compared to a static menu entry, but makes editing while in GRUB a bit more convenient as defaults are hidden away.

Explanation of `make_liveentry` usage:

```
make_liveentry '[AMD64] Linux Mint 18.3 [Cinnamon Edition] +Persist' \
    '/iso/linuxmint-18.3-cinnamon-64bit.iso' losetup \
    set gfxpayload=keep \
    linuxlive_casper /casper/vmlinuz persistent-path=/persist/Mint_18_3_Cinnamon/ \
    initrd /casper/initrd.lz
```

builds an equivalent menuentry like this:

```
menuentry '[AMD64] Linux Mint 18.3 [Cinnamon Edition] +Persist' --unrestricted {
    set path_iso='/iso/linuxmint-18.3-cinnamon-64bit.iso'
    losetup loopiso "${path_iso}"
    set gfxpayload=keep
    linux (loopiso)/casper/vmlinuz iso-scan/filename="${path_iso}" boot=casper preseed/file=/cdrom/preseed/linuxmint.seed quiet splash persistent persistent-path=/persist/Mint_18_3_Cinnamon/ noprompt --
    initrd (loopiso)/casper/initrd.lz
}
```

`make_liveentry` shortens the amount of boilerplate configuration and checks for the presense of the ISO file.  To do so, it takes 10 arguments, broken down here into 5 segments for easier understanding:

- menu entry name
   - name of menu entry
- ISO setup
   - path to ISO file from root of partition
   - loopback setup function
- preparation
   - preparation function
   - arguments to preparation function
- kernel setup
   - kernel setup function
   - kernel path from root of ISO
   - arguments to kernel setup function
- initrd setup
   - initrd setup function
   - initrd path from root of ISO

Functions can be anything that GRUB2 can execute, so either built-in GRUB2 commands or user-defined functions.  In the example above, `linuxlive_casper` is a function defined in `conf.d/livefunc.cfg`, while `initrd` is the built-in GRUB2 command.

The user-defined kernel setup functions parse the arguments sent to them so that useful defaults are there to reduce the amount of options that have to be explicitly specified.  Note that because of this, whitespace can cause issues.  To bypass parsing, the arguments can be split, like `parsed -- literal`, so that the literal portion is appended to the list of boot parameters as-is.

### Tips

After booting a system, if it's Linux, look at `/proc/cmdline` for the kernel parameters that were passed from the bootloader, in case you want to know all parameters passed, including the hidden ones that don't show in the printed additional boot parameters.

### casperplus

`casperplus` and `casper+.cpio` is from [`tiny-tools` project](https://gitlab.com/serenascopycat/tiny-tools).  It's an optional tweak to the system via `initrd`.

## More information

Currently, information is available for:

- [X] Ubuntu
   - [X] Linux Mint
   - [X] Peppermint (up to 10 Respin)
   - [X] Pop! OS
- [X] Debian
   - [ ] Devuan
      - [X] Forge OS
   - [X] Kali
   - [X] Parrot OS
   - [X] Tails
   - [X] LMDE (Linux Mint Debian Edition)
- [ ] Red Hat
   - [X] Fedora
   - [X] CentOS (install, not live)
- [X] Arch Linux
   - [X] EndeavourOS
   - [X] BBQLinux
   - [ ] Manjaro
      - [X] mAid
- [X] Android x86
   - [X] Bliss OS
   - [X] RemixOS
- [X] Knoppix
- [X] antiX
- [X] Tiny Core
- [ ] Slackware
   - [X] Porteus

At the back of the closet (no direct support; needs work):

- [ ] Alpine
- [ ] Mageia

And some in the queue to be looked at:

- [ ] Artix
- [ ] openSUSE

See this project's [wiki](https://gitlab.com/serenascopycat/grub-config/-/wikis/home).

[List of Live CDs on Wikipedia](https://en.wikipedia.org/wiki/List_of_live_CDs)
